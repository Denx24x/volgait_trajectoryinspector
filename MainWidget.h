#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#endif // MAINWIDGET_H
#include <QWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui (new Ui::MainWindowClass)
{
    ui->setupUi(this);
    ui->pushButton->setText("GO");
    //Connecting a GO pushButton to a func which saves your name into a .txt file
    auto s = ui->lineEdit->text();
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(on_some_pushButton_clicked(s)));
}

void MainWindow::on_some_pushButton_clicked(QString s)
{
    FILE *file;
    file=fopen("data.txt", "wt");
    fprintf(file, "XD\n");
    auto z=(s.toStdString()).c_str();
    fprintf(file, "%s", z);
    fclose(file);
}
