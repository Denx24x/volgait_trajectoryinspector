#include <QGuiApplication>

#include <Qt3DRender/qcamera.h>
#include <Qt3DCore/qentity.h>
#include <Qt3DRender/qcameralens.h>

#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLayoutItem>
#include <QObject>
#include <QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QCommandLinkButton>
#include <QtGui/QScreen>

#include <Qt3DInput/QInputAspect>

#include <Qt3DRender/qmesh.h>
#include <Qt3DRender/qtechnique.h>
#include <Qt3DRender/qmaterial.h>
#include <Qt3DRender/qeffect.h>
#include <Qt3DRender/qtexture.h>
#include <Qt3DRender/qrenderpass.h>
#include <Qt3DRender/qsceneloader.h>
#include <Qt3DRender/qpointlight.h>
#include <Qt3DRender/qrenderaspect.h>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QGeometry>

#include <Qt3DCore/qtransform.h>
#include <Qt3DCore/qaspectengine.h>
#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DExtras/qforwardrenderer.h>
#include <Qt3DExtras/qt3dwindow.h>
#include <Qt3DExtras/qfirstpersoncameracontroller.h>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QOrbitCameraController>
#include <Qt3DExtras/QText2DEntity>


#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <stdio.h>
#include <iostream>
#include <string>
#include <algorithm>
#include <math.h>
#include <vector>
#include "mainwindow.h"
#include <Qt3DRender/QCamera>

using namespace std;

// преобразование градусы <-> радианы
#define degreesToRadians(angleDegrees) ((angleDegrees) * 3.14 / 180.0)
#define radiansToDegrees(angleRadians) ((angleRadians) * 180.0 / 3.14)

struct point{
    double x_ang;
    double y_ang;
    QVector3D pos;
    double distance;
    point(){}
    point(double a, double b, QVector3D c, double d){
        x_ang = a;
        y_ang = b;
        pos = c;
        distance = d;
    }
};

// глобальные переменные
vector<point> points;

vector<Qt3DCore::QEntity*> used_lines;
MainWindow *widget;

QSlider *trajectory_slider;
QSlider *zoom_slider;

QLabel *trajectory_title;
QLabel *zoom_title;
QLabel *status;
QLabel *pos_title;
QLabel *ang_title;
QLabel *depth_title ;

int max_delta;

Qt3DCore::QEntity *rootEntity = new Qt3DCore::QEntity();
Qt3DRender::QCamera *cameraEntity;
Qt3DCore::QEntity *pointer = new Qt3DCore::QEntity(rootEntity);
//Qt3DExtras::QText2DEntity *texts[3];

Qt3DCore::QEntity* drawLine(const QVector3D& start, const QVector3D& end, const QColor& color, Qt3DCore::QEntity *_rootEntity)
{
    auto *geometry = new Qt3DRender::QGeometry(_rootEntity);

    // position vertices (start and end)
    QByteArray bufferBytes;
    bufferBytes.resize(3 * 2 * sizeof(float)); // start.x, start.y, start.end + end.x, end.y, end.z
    float *positions = reinterpret_cast<float*>(bufferBytes.data());
    *positions++ = start.x();
    *positions++ = start.y();
    *positions++ = start.z();
    *positions++ = end.x();
    *positions++ = end.y();
    *positions++ = end.z();

    auto *buf = new Qt3DRender::QBuffer(geometry);
    buf->setData(bufferBytes);

    auto *positionAttribute = new Qt3DRender::QAttribute(geometry);
    positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
    positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    positionAttribute->setVertexSize(3);
    positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    positionAttribute->setBuffer(buf);
    positionAttribute->setByteStride(3 * sizeof(float));
    positionAttribute->setCount(2);
    geometry->addAttribute(positionAttribute); // We add the vertices in the geometry

    // connectivity between vertices
    QByteArray indexBytes;
    indexBytes.resize(2 * sizeof(unsigned int)); // start to end
    unsigned int *indices = reinterpret_cast<unsigned int*>(indexBytes.data());
    *indices++ = 0;
    *indices++ = 1;

    auto *indexBuffer = new Qt3DRender::QBuffer(geometry);
    indexBuffer->setData(indexBytes);

    auto *indexAttribute = new Qt3DRender::QAttribute(geometry);
    indexAttribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    indexAttribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    indexAttribute->setBuffer(indexBuffer);
    indexAttribute->setCount(2);
    geometry->addAttribute(indexAttribute); // We add the indices linking the points in the geometry

    // mesh
    auto *line = new Qt3DRender::QGeometryRenderer(_rootEntity);
    line->setGeometry(geometry);
    line->setPrimitiveType(Qt3DRender::QGeometryRenderer::Lines);
    auto *material = new Qt3DExtras::QPhongMaterial(_rootEntity);
    material->setAmbient(color);

    // entity
    auto *lineEntity = new Qt3DCore::QEntity(_rootEntity);
    lineEntity->addComponent(line);
    lineEntity->addComponent(material);
    return lineEntity;
}

void draw(vector<point> &arr, QColor clr){
    for(int i = 1; i < arr.size();i++){ // прорисовка результата
        used_lines.push_back(drawLine(arr[i - 1].pos, arr[i].pos, clr, rootEntity));
    }
}

void update_text(){
    if(points.size() == 0)
        return;
    vector<point> &arr = points;
    int val = trajectory_slider->value();
    /*
    auto *textTransform = new Qt3DCore::QTransform(texts[0]);
    textTransform->setRotation(cameraEntity->transform()->rotation() * -1);
    textTransform->setTranslation(arr[val].pos);
    textTransform->setScale(0.05);
    texts[0]->addComponent(textTransform);
    */
    pos_title->setText("Pos: x " + QString::number(arr[val].pos.x()) + " y " + QString::number(arr[val].pos.y()) + " z " + QString::number(arr[val].pos.z()));
    ang_title->setText("Ang: az " + QString::number(arr[val].x_ang) + " inc " + QString::number(arr[val].y_ang));
    depth_title->setText("Depth: " + QString::number(arr[val].distance));
}

void on_trajectory_slider_valueChanged(int val){
    vector<point> &arr = points;
    cameraEntity->setPosition(cameraEntity->position() - cameraEntity->viewCenter() + arr[val].pos);
    cameraEntity->setViewCenter(arr[val].pos);
    auto *new_transform = new Qt3DCore::QTransform(pointer);
    new_transform->setTranslation(arr[val].pos);
    pointer->addComponent(new_transform);
    update_text();
}
void on_zoom_slider_valueChanged(int val){
    cameraEntity->translate(QVector3D(0, 0, -val+(cameraEntity->viewCenter() - cameraEntity->transform()->translation()).length()), cameraEntity->DontTranslateViewCenter);
}

vector<point> generate(QJsonArray arr){
    vector<point> data_array;
    data_array.push_back(point(0, 0, QVector3D(0, 0, 0), 0));
    for(int i = 0; i < arr.count(); i++){ // для каждой точки
        QJsonValue bf = arr.at(i);
        double x_ang, y_ang, l;
        x_ang = bf["azimuth"].toDouble();
        y_ang = bf["inclination"].toDouble();
        l = bf["depth"].toDouble(); // получение значений
        double change_l = l - data_array[data_array.size() - 1].distance; // изменение расстояния
        double last_x = data_array[data_array.size() - 1].x_ang;
        double last_y = data_array[data_array.size() - 1].y_ang;
        QVector3D angle = QVector3D(x_ang, y_ang, 0); // текущий угол
        double ost_distance = change_l;

        angle = QVector3D(last_x, last_y, 0);
        double dls_distance = 10; // длина шага (чем меньше тем больше сглаженность)
        double dls = degreesToRadians(dls_distance / (change_l * ((sin(last_y) * sin(y_ang)) * (sin(x_ang) * sin(last_x) + cos(last_x) * cos(x_ang)) + cos(y_ang) * cos(last_y))));
        double ang_distance = sqrt(pow(x_ang - last_x, 2) + pow(y_ang - last_y, 2));
        QVector3D ang_vector = QVector3D((x_ang - last_x) / ang_distance, (y_ang - last_y) / ang_distance, 0); // текущее направление угла

        // моделирование отрезка с шагом dls_distance и смещением angle на ang_vector * dls
        for(int g = 0; g < int(change_l / dls_distance);g++){
            angle += ang_vector * dls;
            double x, y, z;
            QVector3D last_pos = data_array[data_array.size() - 1].pos;
            z = last_pos.z() + dls_distance * sin(angle.x()) * cos(angle.y());
            x = last_pos.x() + dls_distance * sin(angle.x()) * sin(angle.y());
            y = last_pos.y() - dls_distance * cos(angle.y()); // нахождение новой координаты (по сфере)
            data_array.push_back(point(angle.x(), angle.y(), QVector3D(x, y, z), data_array[data_array.size() - 1].distance + dls_distance)); // запись
        }
        double dst = change_l - int(change_l / dls_distance) * dls_distance;
        angle += ang_vector * dls * (dst / dls_distance);


        // последняя точка отрезка
        double x, y, z;
        QVector3D last_pos = data_array[data_array.size() - 1].pos;
        z = last_pos.z() + ost_distance * sin(angle.x()) * cos(angle.y());
        x = last_pos.x() + ost_distance * sin(angle.x()) * sin(angle.y());
        y = last_pos.y() - ost_distance * cos(angle.y()); // нахождение новой координаты (по сфере)
        data_array.push_back(point(angle.x(), angle.y(), QVector3D(x, y, z), data_array[data_array.size() - 1].distance + ost_distance)); // запись
    }
    return data_array; // все полученные точки
}

void on_data_button_clicked(){ // нажание на кнопку загрузки файла
    QFileDialog dialog;
    QString filename = dialog.getOpenFileName(widget,
        "Open file", "", "Json Files (*.json)"); // спросить файл
    if(!dialog.Accept)
        return;
    QFile loadFile(filename); // загрузить файл
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open file.");
        return;
    }
    points.clear();
    for(int i = 1; i < used_lines.size();i++){ // удаление старого графика
        used_lines[i]->deleteLater();
    }
    used_lines.clear();
    QByteArray sdata = loadFile.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(sdata);
    QJsonArray arr = doc.array();
    points = generate(arr);
    trajectory_slider->setValue(0);
    trajectory_slider->setMaximum(points.size() - 1);
    draw(points, QColor(QRgb(0x000000)));
    status->setText(filename);

    QVector3D mn = points.size() ? points[0].pos : QVector3D(0, 0, 0);
    QVector3D mx = points.size() ? points[0].pos : QVector3D(0, 0, 0);

    for(int i = 0; i < points.size();i++){
        mn.setX(min(mn.x(), points[i].pos.x()));
        mn.setY(min(mn.y(), points[i].pos.y()));
        mn.setZ(min(mn.z(), points[i].pos.z()));
        mx.setX(max(mx.x(), points[i].pos.x()));
        mx.setY(max(mx.y(), points[i].pos.y()));
        mx.setZ(max(mx.z(), points[i].pos.z()));
    }
    //cameraEntity->translate(QVector3D(0, 0, 100));
    max_delta = max(max(abs(mx.x() - mn.x()), abs(mx.y() - mn.y())), abs(mx.z() - mn.z())) / (sqrt(3) / 3) + 300;
    int pos = 0;
    float delta = max_delta;
    for(int i = 0; i < points.size();i++){
        if(abs(abs(mn.y() - points[i].pos.y()) - abs(mx.y() - points[i].pos.y())) < delta){
            pos = i;
            delta = abs(abs(mn.y() - points[i].pos.y()) - abs(mx.y() - points[i].pos.y()));
        }
    }
    trajectory_slider->setValue(pos);
    zoom_slider->setMaximum(max_delta);
    zoom_slider->setValue(max_delta);
    //int val = points.size() / 2;
    //cameraEntity->setPosition(cameraEntity->position() - cameraEntity->viewCenter() + points[val].pos);
    //cameraEntity->setViewCenter(points[val].pos);
    //cameraEntity->setViewCenter(QVector3D(0, len / 2, 0));
    return;
}

float clampInputs(float input1, float input2)
{
    float axisValue = input1 + input2;
    return (axisValue < -1) ? -1 : (axisValue > 1) ? 1 : axisValue;
}


class OrbitController : public Qt3DExtras::QAbstractCameraController  // цыганский фокус
{
    void moveCamera(const QAbstractCameraController::InputState &state, float dt) override;
};

void OrbitController::moveCamera(const QAbstractCameraController::InputState &state, float dt) // кастомное управление
{
    Qt3DRender::QCamera *theCamera = camera();
    if (theCamera == nullptr)
        return;
    theCamera->panAboutViewCenter((clampInputs(state.rxAxisValue, state.txAxisValue) * lookSpeed()) * dt, QVector3D(0.0f, 1.0f, 0.0f)); // чтобы крутить
    theCamera->tiltAboutViewCenter((clampInputs(state.ryAxisValue, state.tyAxisValue) * lookSpeed()) * dt); // чтобы вращать
    theCamera->translate(QVector3D(0, 0, linearSpeed() * state.tzAxisValue * dt), theCamera->DontTranslateViewCenter); // чтобы двигать вдоль
    update_text();
}

int main(int argc, char **argv)
{
    setlocale(0, "");
    QApplication app(argc, argv);
    widget = new MainWindow();

    // инициализация 3д
    Qt3DExtras::Qt3DWindow *view = new Qt3DExtras::Qt3DWindow();
    view->defaultFrameGraph()->setClearColor(QColor(QRgb(0x4d4d4f)));
    view->setRootEntity(rootEntity);
    QWidget *container = QWidget::createWindowContainer(view);
    container->setMinimumSize(QSize(400, 400));

    // разметка со всякими штуками
    QVBoxLayout *hLayout = new QVBoxLayout(widget);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->setAlignment(Qt::AlignBottom);
    hLayout->addWidget(container, 1);
    hLayout->addLayout(vLayout);

    // камера
    cameraEntity = view->camera();
    cameraEntity->lens()->setPerspectiveProjection(60.0f, 16.0f/9.0f, 0.0001f, -1.0f);
    cameraEntity->setUpVector(QVector3D(0, 1, 0));
    cameraEntity->setViewCenter(QVector3D(0, 0, 0));
    cameraEntity->setPosition(QVector3D(100, 100, 100));

    // контроллер камеры
    OrbitController * controller = new OrbitController();
    controller->setParent(rootEntity);
    controller->setCamera(cameraEntity);
    controller->setLookSpeed( 180.0f );
    controller->setLinearSpeed(200.0f);

    QHBoxLayout *shLayout = new QHBoxLayout();
    vLayout->addLayout(shLayout);

    // update data button
    QPushButton *data_button = new QPushButton("Выбрать файл");
    QObject::connect(data_button, &QPushButton::pressed, on_data_button_clicked);
    shLayout->addWidget(data_button);
    status = new QLabel();
    status->setText(QString("Данные не загружены"));
    shLayout->addWidget(status);

    QHBoxLayout *sh2Layout = new QHBoxLayout();
    vLayout->addLayout(sh2Layout);
    pos_title = new QLabel();
    sh2Layout->addWidget(pos_title);
    ang_title = new QLabel();
    sh2Layout->addWidget(ang_title);
    depth_title = new QLabel();
    sh2Layout->addWidget(depth_title);

    trajectory_title = new QLabel(QString("Перемещение вдоль траектории"));
    hLayout->addWidget(trajectory_title);

    // штука чтобы двигаться вдоль траектории
    trajectory_slider = new QSlider();
    QObject::connect(trajectory_slider, &QSlider::valueChanged, on_trajectory_slider_valueChanged);
    trajectory_slider->setMinimum(0);
    trajectory_slider->setMaximum(0);
    trajectory_slider->setValue(0);
    trajectory_slider->setOrientation(Qt::Horizontal);
    hLayout->addWidget(trajectory_slider);



    zoom_title = new QLabel(QString("Отдаление"));
    hLayout->addWidget(zoom_title);

    // штука чтобы двигаться вдоль траектории
    zoom_slider = new QSlider();
    QObject::connect(zoom_slider, &QSlider::valueChanged, on_zoom_slider_valueChanged);
    zoom_slider->setMinimum(20);
    zoom_slider->setMaximum(20);
    zoom_slider->setValue(20);
    zoom_slider->setOrientation(Qt::Horizontal);
    hLayout->addWidget(zoom_slider);

    // координатные оси
    drawLine(QVector3D(1000, 0, 0), QVector3D(-1000, 0, 0), QColor(QRgb(0xff0000)), rootEntity);
    drawLine(QVector3D(0, 1000, 0), QVector3D(0, -1000, 0), QColor(QRgb(0x00ff00)), rootEntity);
    drawLine(QVector3D(0, 0, 1000), QVector3D(0, 0, -1000), QColor(QRgb(0x0000ff)), rootEntity);

    // указатель (координатные оси по-меньше)
    drawLine(QVector3D(10, 0, 0), QVector3D(0, 0, 0), QColor(QRgb(0xff0000)), pointer);
    drawLine(QVector3D(0, 10, 0), QVector3D(0, 0, 0), QColor(QRgb(0x00ff00)), pointer);
    drawLine(QVector3D(0, 0, 10), QVector3D(0, 0, 0), QColor(QRgb(0x0000ff)), pointer);
    /*
    for(int i = 0; i < 3;i++){
        texts[i] = new Qt3DExtras::QText2DEntity(i > 0 ? texts[i - 1] : rootEntity);
        texts[i]->setHeight(20);
        texts[i]->setWidth(200);
        auto *textTransform = new Qt3DCore::QTransform(texts[0]);
        textTransform->setTranslation(QVector3D(0, i > 0 ? -20 : 0, 0));
        texts[i]->addComponent(textTransform);
    }
    */
    //widget->show();
    widget->showMaximized();
    //widget->showNormal();
    //widget->resize(600, 600);

    return app.exec();
}
